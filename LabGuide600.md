# Create Web App to Manage Push Notifications

![](images/600/title.png)  
Update: March 31, 2017

## Introduction

This is the third of several labs that are part of the Oracle Public Cloud **AMCe Application Development workshop.** This workshop will walk you through the Software Development Lifecycle (SDLC) for a dual-channel application (web + mobile) built using Oracle's Autonomous Mobile Cloud Enterprise (AMCe) as a complete backend solution.

In the first lab (100), you created a new Mobile Backend (MBE) project and configured your Autonomous Mobile Cloud Enterprise (AMCe) environment. In the second lab (200), you created, populated, and tested your MBE's storage collections. In this lab, you will use the AMCe API Platform to design and deploy a custom API.

***To log issues***, click here to go to the [github oracle](https://github.com/oracle/cloud-native-devops-workshop/issues/new) repository issue submission form.

## Objectives
- Create Initial Custom API
- Create New Resource Endpoints
- Add HTTP Request Methods to Resource Endpoints
- Install AMCe CLI Tools
- Upload Implmentation Code
- Test API Implementation

## Required Artifacts
- The following lab requires an Oracle Public Cloud account that will be supplied by your instructor.

# Create Mobile Client Project

## Create React Native Project

## Add Project Implementation

## Build Working Application